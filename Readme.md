# handson-vscode-antlr-extension

Try the steps in this site.

[ANTLRを使って構文解析を行う - Qiita](https://qiita.com/i-tanaka730/items/4eeaae247f70895c3456)

## Requirement

VSCode extension "ANTLR4 grammar syntax support"(mike-lischke.vscode-antlr4)

## Usage

In VSCode:

1. Create grammar file
    - Edit './playground/testgrammer.g4'
2. Visualize grammar file and check
    - Open launch.json and debug by pressing "F5" key.  
        Parse tree diagram should show up.
    - Check if the diagram is as expected.
3. Repeat 1 and 2 and complete grammar file
4. Use parser and lexer files
    - Parser and lexer files are created in './playground/output/generated-code/' when grammar file is saved.  
        You can take it away and use them anywhere.
